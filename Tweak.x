#include "libfinder/LFFinderController.h"

%hook TabbedLibraryViewController
-(void)setViewControllers:(NSArray*)controllers animated:(BOOL)animated {
  for (LFFinderController* sub in [self viewControllers]){
    if([sub isKindOfClass:[LFFinderController class]]){return %orig;}
  }
  LFFinderController* finder=[[LFFinderController alloc] init];
  [finder.tabBarItem=[[UITabBarItem alloc]
   initWithTabBarSystemItem:UITabBarSystemItemMore
   tag:controllers.count] release];
  %orig([controllers arrayByAddingObject:finder],animated);
  [finder release];
}
%end
