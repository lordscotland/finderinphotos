ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = FinderInPhotos
FinderInPhotos_FILES = Tweak.x
FinderInPhotos_FRAMEWORKS = UIKit
FinderInPhotos_LDFLAGS = -Llibfinder -lfinder

include $(THEOS_MAKE_PATH)/tweak.mk
